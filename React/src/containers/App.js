import React, {PureComponent} from 'react';
import {Route, Switch, HashRouter} from 'react-router-dom'
import '../styles/bootstrap.css';
import UserManager from '../Pages/UserPages/UserManager';
import CreateUserPage from '../Pages/UserPages/CreateUserPage';
import EditUserPage from '../Pages/UserPages/EditUserPage';
import ProjectManager from '../Pages/ProjectPages/ProjectManager';
import CreateProjectPage from '../Pages/ProjectPages/CreateProjectPage';
import EditProjectPage from '../Pages/ProjectPages/EditProjectPage';
import ProjectDetails from '../Pages/ProjectPages/ProjectDetails';

class App extends PureComponent {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <HashRouter>
        <Switch>
        <Route path="/" exact component={ProjectManager} key={'ProjectManager'}/>,
        <Route path="/usermanager" exact component={UserManager} key={'UserManager'}/>,
        <Route path="/adduser" exact component={CreateUserPage} key={"AddUser"} />,
        <Route path="/edituser" component={EditUserPage} key={"EditUser"} />,
        <Route path="/projects" exact component={ProjectManager} key={'ProjectManager'}/>,
        <Route path="/addproject" exact component={CreateProjectPage} key={'AddProject'}/>,
        <Route path="/editproject" component={EditProjectPage} key={'EditProject'}/>,
        <Route path="/project" component={ProjectDetails} key={'ProjectDetails'} />,
        </Switch>
      </HashRouter>
    )
  }
  
}

export default App;
