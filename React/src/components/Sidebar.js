import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import { NavLink, withRouter } from 'react-router-dom';

class SideBar extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Menu>
                 <NavLink className="menu-item" to="/usermanager" key="usermanager">
                    Управление сотрудниками
                </NavLink>,
                <NavLink className="menu-item" to="/projects" key="projects">
                    Управление проектами
                </NavLink>
            </Menu>
        )
    }
}
export default withRouter(SideBar);