import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import Home from '@material-ui/icons/Home';
import SideBar from './Sidebar';

class Header extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div>
            <SideBar />
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <NavLink className="btn btn-primary" style={{marginLeft:75}} to="/"><Home /></NavLink>
                    </ul>
                </div>
            </nav>
            </div>
        )
    }
        
}
export default Header;