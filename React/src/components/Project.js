import React, { Component } from 'react';
import '../styles/User.css';
import { TableRow, TableCell, Tooltip, Button } from '@material-ui/core';
import Create from '@material-ui/icons/Create';
import { withRouter } from 'react-router-dom';
import DeleteModal from './Modal/DeleteModal';


class Project extends Component {

    state = {
        id: this.props.item.id,
        action: null
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
        this.setState({ action: event.target.value });
    }

    actionHandler(event, id, action) {
        event.preventDefault();
        this.props.history.push('/' + action + '/' + id);
    }

    render() {
        return (
            <TableRow key={this.props.item.id}>
                <TableCell>{this.props.item.projectName}</TableCell>
                <TableCell>{this.props.item.companyCustomer}</TableCell>
                <TableCell>{this.props.item.companyPerformer}</TableCell>
                <TableCell>{this.props.item.projectManager}</TableCell>
                <TableCell>{this.props.item.beginTime}</TableCell>
                <TableCell>{this.props.item.endTime}</TableCell>
                <TableCell>{this.props.item.priority}</TableCell>
                
                <TableCell className="buttonPadding"
                           align="center"
                >
                    <Tooltip title="Редактировать">
                        <Button type="button"
                                variant="contained"
                                onClick={event => this.actionHandler(event, this.props.item.id, 'editproject')}
                        >
                            <Create />
                        </Button>
                    </Tooltip>
                </TableCell>
                <TableCell className="buttonPadding"
                           align="center"
                >
                    <DeleteModal item={this.props.item}
                                     handlerRefresh={this.props.handlerRefresh}
                                     path="https://localhost:44393/api/deleteProject"
                    />

                </TableCell>
            </TableRow>
        )
    }
}

export default withRouter(Project);