import React, { Component } from 'react';
import User from '../../components/User';
import { Container, CssBaseline, Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem, ListItemText, Tooltip, Button, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import '../../styles/usermanager.css'
import Axios from 'axios';

class UserManager extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            fullname: "",
            status: this.props.location.pathname.split('/'),
            isUpdateState: false
        }
    }

    componentDidMount() {
        Axios.get('/api/getAllWorkers')
            .then(response => this.setState({ users: response.data }));
    }

    componentDidUpdate() {
        if(this.state.isUpdateState){
            Axios
                .get('/api/getAllWorkers')
                .then(response => this.setState({ users: response.data }))
                .catch(err => console.log(err));

                this.setState({ isUpdateState: false })
        }
    }

    handlerRefresh() {
        this.setState({ isUpdateState: true })
    }

    handleChange(event) {
        event.preventDefault();
        else {
            this.setState({ [event.target.name]: event.target.value })
        }
    }
    
    userAddHandler() {
        this.props.history.push({
            pathname: '/adduser'
        });
    }

    submitSearch(event) {
        var roles = this.state.selectedRole;
        event.preventDefault();
        Axios.get('/api/getAllWorker?name='
		+this.state.name)
                .then(response => this.setState({ users: response.data }))
                .catch(err => console.log(err));
    }

    render() {
        const { fullname } = this.state;
        return (
            <div>
                <Container component="main" maxWidth="lg" className="container">
                    <CssBaseline />
                    <form onSubmit={this.submitSearch.bind(this)}>
                        <Grid container spacing={6}>
                            <Grid item xs={12} sm={3}>
                                <TextField name="fullname"
                                           variant="outlined"
                                           id="fullname"
                                           label="Поиск по ФИО"
                                           value={fullname}
                                           style={{marginTop: 7}}
                                           onChange={this.handleChange.bind(this)}
                                           fullWidth
                                />
                            </Grid>
                            <Grid item xs={6} sm={2}>
                                <Tooltip title="Поиск">
                                    <Button type="submit"
                                            variant="contained"
                                            color="primary"
                                            fullWidth
                                            style={{marginTop: 20}}
                                    >
                                        <Search />
                                    </Button>
                                </Tooltip>
                            </Grid>
                            <Grid item xs={6} sm={1}>
                                <Tooltip title="Добавить пользователя">
                                    <Button type="button"
                                            onClick={this.userAddHandler.bind(this)}
                                            variant="contained"
                                            color="primary"
                                            fullWidth
                                            style={{marginTop: 20}}
                                    >
                                        <AddIcon />
                                    </Button>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </form>
                    <Grid container spacing={4}>
                        <Grid item xs={12} sm={12}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Имя</TableCell>
                                        <TableCell>Email</TableCell>
                                        <TableCell>Роль</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.users.map(user => (
                                        <User key={user.id} item={user} handlerRefresh={this.handlerRefresh.bind(this)}/>
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default UserManager;