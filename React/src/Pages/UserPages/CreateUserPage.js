import React from 'react';
import CreateFormTheme from '../../styles/CreatePageStyles';
import generatePassword from 'generate-password';
import CustomSnackBar from '../../components/SnackBar';
import { Redirect } from 'react-router-dom';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem, Checkbox, ListItemText, FormHelperText, InputAdornment, IconButton, Button } from '@material-ui/core';
import Settings from '@material-ui/icons/Settings';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Axios from 'axios';


class CreateUserPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            surname: '',
            middlename: '',
            email: '',
            requestStatus: false,
        }
    }

    uri = this.props.location.pathname.split('/');

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }

    handleClick = event => {
        event.preventDefault();
        this.props.history.push('/usermanager');
    }

    submitForm(event) {
        event.preventDefault();
            Axios.post('/api/addWorker', {
              name: this.state.firstName,
              surname: this.state.surname,
              email: this.state.email,
            })
            .then(response => this.setState({snackbarMessage:'Изменения сохранены. Вы будете перенаправлены.', 
            snackbarVariant: 'success'}, () => this.refs.child.handleOpen(), setTimeout(() => this.setState({ requestStatus: true }), 2000)))
              .catch(error =>
                this.setState({
                  snackbarMesssage: 'Данный email уже используется',
                  snackbarVariant: 'error'
                }, () => this.refs.child.handleOpen()))
          }

    render() {
        const { classes } = this.props;
        const { firstName, surname,  email, 
             requestStatus, snackbarMessage, snackbarVariant} = this.state;

        return (
                requestStatus ? <Redirect to='/usermanager' /> : (
                    <div>
                        <CustomSnackBar ref="child" message={snackbarMessage} variant={snackbarVariant} />
                        <Container component="main" maxWidth="xs">
                            <CssBaseline />
                            <div className={classes.papper}>
                                <Avatar className={classes.avatar}>
                                    <Settings />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Добавление пользователя
                                </Typography>
                                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12} sm={6}>
                                            <TextField name="firstName"
                                                       variant="outlined"
                                                       value={firstName}
                                                       required
                                                       fullWidth
                                                       id="firstName"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Имя"
                                                       autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField name="surname"
                                                       variant="outlined"
                                                       value={surname}
                                                       required
                                                       fullWidth
                                                       id="surname"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Фамилия"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="middlename"
                                                       variant="outlined"
                                                       value={middlename}
                                                       fullWidth
                                                       id="middlename"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Отчество"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="email"
                                                       variant="outlined"
                                                       value={email}
                                                       required
                                                       fullWidth
                                                       id="email"
                                                       type="email"
                                                       onChange={this.handleChange.bind(this)}
                                                       label="Email(Логин)"
                                                       
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                    type="submit"
                                                    color="primary"
                                                    variant="contained"
                                                    className={classes.submit}
                                            >
                                                Добавить
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                    onClick={this.handleClick}
                                                    variant="contained"
                                                    className={classes.submit}
                                            >
                                                Отменить
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </Container>
                    </div>
                )
            )
    }
}
CreateUserPage.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(CreateFormTheme)(CreateUserPage);
