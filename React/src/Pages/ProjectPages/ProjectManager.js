import React, { Component } from 'react';
import { Container, 
    CssBaseline, 
    Grid, 
    TextField, 
    Tooltip, 
    Button, 
    Table, 
    TableHead, 
    TableRow, 
    TableCell, 
    TableBody, MenuItem,Select,Input,FormControl,InputLabel } from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import '../../styles/usermanager.css';
import Project from '../../components/Project';
import Axios from 'axios';
import Header from '../../components/Header';

class ProjectManager extends Component {

    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            name: "",
            customerCompanyName: "",
            executingCompanyName: "",
            superVisorProjectName: "",
            priority: "", 
            isUpdateState: false
        }
    }

    componentDidMount() {
        Axios.get('https://localhost:44393/api/getAllProjects')
            .then(response => this.setState({ projects: response.data }));
    }

    componentDidUpdate() {
        if (this.state.isUpdateState) {
            Axios.get('https://localhost:44393/api/getAllProjects')
            .then(response => this.setState({ projects: response.data }));
            this.setState({ isUpdateState: false });
        }
    }

    handlerRefresh() {
        this.setState({ isUpdateState: true })
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }

    projectAddHandler() {
        this.props.history.push({
            pathname: '/addproject'
        });
    }

    submitSearch(event) {
        event.preventDefault();
        Axios.get('https://localhost:44393/api/getAllProjects?name='
        + this.state.name
        + '&customerCompanyName=' + this.state.customerCompanyName
        + '&executingCompanyName=' + this.state.executingCompanyName
        + '&supervisorProjectName=' + this.state.superVisorProjectName
        + '&priority='+ this.state.priority)
            .then(response => this.setState({ projects: response.data}))
            .catch(err => console.log(err));
    }

    render() {
        const { name, customerCompanyName, executingCompanyName, superVisorProjectName, priority } = this.state;
        return (
            <div>
                <Header/>
                <Container component="main" maxWidth="xl" className="container">
                    <CssBaseline />
                    <form onSubmit={this.submitSearch.bind(this)}>
                        <Grid container spacing={6}>
                            <Grid item xs={12} sm={3}>
                                <TextField name="name"
                                    variant="outlined"
                                    id="name"
                                    label="Название проекта"
                                    value={name}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="customerCompanyName"
                                    variant="outlined"
                                    id="customerCompanyName"
                                    label="Имя заказчика"
                                    value={customerCompanyName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="executingCompanyName"
                                    variant="outlined"
                                    id="executingCompanyName"
                                    label="Имя исполнителя"
                                    value={executingCompanyName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField name="superVisorProjectName"
                                    variant="outlined"
                                    id="superVisorProjectName"
                                    label="Имя руководителя"
                                    value={superVisorProjectName}
                                    style={{ marginTop: 7 }}
                                    onChange={this.handleChange.bind(this)}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <FormControl fullWidth>
                                                <InputLabel htmlFor="select-priority">Приоритет</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={priority}
                                                    name="priority"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-priority" />}
                                                >
                                                    <MenuItem value={1}>1</MenuItem>
                                                    <MenuItem value={2}>2</MenuItem>
                                                    <MenuItem value={3}>3</MenuItem>
                                                    <MenuItem value={4}>4</MenuItem>
                                                    <MenuItem value={5}>5</MenuItem>
                                                </Select>
                                            </FormControl>
                            </Grid>
                            <Grid item xs={6} sm={2}>
                                <Tooltip title="Поиск">
                                    <Button type="submit"
                                        variant="contained"
                                        color="primary"
                                        fullWidth
                                        style={{ marginTop: 20 }}
                                    >
                                        <Search />
                                    </Button>
                                </Tooltip>
                            </Grid>
                            <Grid item xs={6} sm={1}>
                                <Tooltip title="Добавить проект">
                                    <Button type="button"
                                        onClick={this.projectAddHandler.bind(this)}
                                        variant="contained"
                                        color="primary"
                                        fullWidth
                                        style={{ marginTop: 20 }}
                                    >
                                        <AddIcon />
                                    </Button>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </form>
                    <Grid container spacing={4}>
                        <Grid item xs={12} sm={12}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Название проекта</TableCell>
                                        <TableCell>Заказчик</TableCell>
                                        <TableCell>Исполнитель</TableCell>
                                        <TableCell>Руководитель</TableCell>
                                        <TableCell>Дата начала</TableCell>
                                        <TableCell>Дата конца</TableCell>
                                        <TableCell>Приоритет</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.projects.map(project => (
                                        <Project key={project.id} item={project} handlerRefresh={() => this.handlerRefresh()} />
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default ProjectManager;