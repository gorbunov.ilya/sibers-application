import React from 'react';
import EditFormTheme from '../../styles/CreatePageStyles';
import CustomSnackBar from '../../components/SnackBar';
import { Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem, ListItemText, Button } from '@material-ui/core';
import Settings from '@material-ui/icons/Settings';
import Axios from 'axios';
import Header from '../../components/Header';
class EditProjectPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            customerCompanyName: '',
            executingCompanyName: '',
            supervisorProjectId: '',
            beginDate: '',
            endDate: '',
            priority: '',
            managers: [],
            executingEmployeesId: [],
            requestStatus: false,
            managerError: '',
            helperStatus: ''
        }
    }

    uri = this.props.location.pathname.split('/');

    componentDidMount() {
        Axios.get('https://localhost:44393/api/getProject?id=' + this.uri[2])
            .then(response => this.setState({
                id: response.data.id,
                name: response.data.projectName,
                customerCompanyName: response.data.companyCustomer,
                executingCompanyName: response.data.companyPerformer,
                supervisorProjectId: response.data.projectManager,
                beginDate: response.data.beginTime,
                endDate: response.data.endTime,
                priority: response.data.priority
            }))
            .catch(error => console.log('Error:', error));
            Axios.get('https://localhost:44393/api/getAllWorkers')
                .then(response => this.setState({managers : response.data}));
    }

    handleChange(event) {
        event.preventDefault();
        if (event.target.name === 'select-manager' && event.target.value !== null) {
            this.setState({ managerError: '' })
        }
        this.setState({ [event.target.name]: event.target.value });
    }

    handleClick = event => {
        event.preventDefault();
        this.props.history.push('/projects');
    }

    submitForm(event) {
        event.preventDefault();
        if (this.state.supervisorProjectId !== null) {
            Axios.post('https://localhost:44393/api/editproject', {
                id: this.state.id,
                name: this.state.name,
                companyCustomer: this.state.customerCompanyName,
                companyPerformer: this.state.executingCompanyName,
                projectManager: this.state.supervisorProjectId,
                beginTime: this.state.beginDate,
                endTime: this.state.endDate,
                priority: this.state.priority,
            })
                .then(response => this.setState({
                    snackbarMessage: response.data,
                    snackbarVariant: 'success'
                }, 
                () => this.refs.child.handleOpen(), 
                setTimeout(() => this.setState({ requestStatus: true }), 2000))
                .catch(error =>
                    this.setState({
                        snackbarMessage: 'Проверьте правильность набора',
                        snackbarVariant: 'error'
                    }), () => this.refs.child.handleOpen(), () => console.log(response.data)))
        }
        else {
            this.setState({ managerError: 'Выберите менеджера проекта' })
        }
    }

    render() {
        const { classes } = this.props;
        const { name, customerCompanyName, executingCompanyName, supervisorProjectId, priority, executingEmployeesId,
            requestStatus, snackbarMessage, snackbarVariant, users } = this.state;
        return (
                requestStatus ? <Redirect to='/projects' /> : (
                    <div>
                        <Header/>
                        <CustomSnackBar ref="child" message={snackbarMessage} variant={snackbarVariant} />
                        <Container component="main" maxWidth="xs">
                            <CssBaseline />
                            <div className={classes.papper}>
                                <Avatar className={classes.avatar}>
                                    <Settings />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Редактирование проекта
                                </Typography>
                                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12} sm={12}>
                                            <TextField name="name"
                                                variant="outlined"
                                                value={name}
                                                required
                                                fullWidth
                                                id="name"
                                                onChange={this.handleChange.bind(this)}
                                                label="Название проекта"
                                                autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <TextField name="customerCompanyName"
                                                variant="outlined"
                                                value={customerCompanyName}
                                                required
                                                fullWidth
                                                id="customerCompanyName"
                                                onChange={this.handleChange.bind(this)}
                                                label="Заказчик"

                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="executingCompanyName"
                                                variant="outlined"
                                                value={executingCompanyName}
                                                fullWidth
                                                required
                                                id="executingCompanyName"
                                                onChange={this.handleChange.bind(this)}
                                                label="Исполнитель"

                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-manager">Менеджер проекта</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={supervisorProjectId}
                                                    name="supervisorProjectId"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-manager" />}
                                                >
                                                    {this.state.managers.map(manager => (
                                                        <MenuItem key={manager.id} value={`${manager.name} ${manager.surname}`}>
                                                            <ListItemText primary={`${manager.name} ${manager.surname}`} />
                                                        </MenuItem>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-priority">Приоритет</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={priority}
                                                    name="priority"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-priority" />}
                                                >
                                                    <MenuItem value={1}>1</MenuItem>
                                                    <MenuItem value={2}>2</MenuItem>
                                                    <MenuItem value={3}>3</MenuItem>
                                                    <MenuItem value={4}>4</MenuItem>
                                                    <MenuItem value={5}>5</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                type="submit"
                                                color="primary"
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Редактировать
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                onClick={this.handleClick}
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Отменить
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </Container>
                    </div>
                )
            )
    }
}
EditProjectPage.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(EditFormTheme)(EditProjectPage);