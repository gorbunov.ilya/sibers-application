import React from 'react';
import CreateFormTheme from '../../styles/CreatePageStyles';
import CustomSnackBar from '../../components/SnackBar';
import { Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Container, CssBaseline, Avatar, Typography, Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem, ListItemText, Button } from '@material-ui/core';
import Settings from '@material-ui/icons/Settings';
import Axios from 'axios';
import Header from '../../components/Header';


class CreateProjectPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            customerCompanyName: '',
            executingCompanyName: '',
            supervisorProjectId: '',
            beginDate: '',
            endDate: '',
            priority: '',
            users: [],
            selectedExecutingEmployees: [],
            requestStatus: false,
            managerError: ''
        }
    }

    uri = this.props.location.pathname.split('/');

    componentDidMount() {
        Axios.get('https://localhost:44393/api/getAllWorkers')
            .then(response => this.setState({ users: response.data }))
            .catch(error => console.log('Error:' + error));
    }

    handleChange(event) {
        event.preventDefault();
        if (event.target.name === 'selectedManager') {
            this.setState({ managerError: '' });
        }
        this.setState({ [event.target.name]: event.target.value });
    }

    handleClick = event => {
        event.preventDefault();
        this.props.history.push('/projects');
    }

    submitForm(event) {
        event.preventDefault();
        if (this.state.supervisorProjectId !== null) {
            Axios.post('https://localhost:44393/api/createProject', {
                projectName: this.state.name,
                companyCustomer: this.state.customerCompanyName,
                companyPerformer: this.state.executingCompanyName,
                projectManager: "this.state.supervisorProjectId",
                beginTime: this.state.beginDate,
                endTime: this.state.endDate,
                priority: this.state.priority,
            })
                .then(response => response.status === 200 ? this.setState({
                    snackbarMessage: response.data,
                    snackbarVariant: 'success'
                }, () => this.refs.child.handleOpen(), 
                setTimeout(() => this.setState({ requestStatus: true }), 2000)) : null)
        }
        else {
            this.setState({ managerError: 'Выберите менеджера проекта' })
        }
    }

    render() {
        const { classes } = this.props;
        const { name, customerCompanyName, executingCompanyName, supervisorProjectId, beginDate, endDate, priority, executingEmployeesId,
            requestStatus, snackbarMessage, snackbarVariant, users } = this.state;

        return (
                requestStatus ? <Redirect to='/projects' /> : (
                    <div>
                        <Header/>
                        <CustomSnackBar ref="child" message={snackbarMessage} variant={snackbarVariant} />
                        <Container component="main" maxWidth="xs">
                            <CssBaseline />
                            <div className={classes.papper}>
                                <Avatar className={classes.avatar}>
                                    <Settings />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Создание проекта
                                </Typography>
                                <form className={classes.form} onSubmit={this.submitForm.bind(this)}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12} sm={12}>
                                            <TextField name="name"
                                                variant="outlined"
                                                value={name}
                                                required
                                                fullWidth
                                                id="name"
                                                onChange={this.handleChange.bind(this)}
                                                label="Название проекта"
                                                autoFocus
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <TextField name="customerCompanyName"
                                                variant="outlined"
                                                value={customerCompanyName}
                                                required
                                                fullWidth
                                                id="customerCompanyName"
                                                onChange={this.handleChange.bind(this)}
                                                label="Заказчик"

                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField name="executingCompanyName"
                                                variant="outlined"
                                                value={executingCompanyName}
                                                fullWidth
                                                required
                                                id="executingCompanyName"
                                                onChange={this.handleChange.bind(this)}
                                                label="Исполнитель"

                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-manager">Менеджер проекта</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={supervisorProjectId}
                                                    name="supervisorProjectId"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-manager" />}
                                                >
                                                    {this.state.users.map(manager => (
                                                        <MenuItem key={manager.id} value={manager.fullName}>
                                                            <ListItemText primary={manager.fullname} />
                                                        </MenuItem>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField name="beginDate"
                                                variant="outlined"
                                                value={beginDate}
                                                fullWidth
                                                id="beginDate"
                                                required
                                                onChange={this.handleChange.bind(this)}
                                                label="Дата начала"
                                                type="date"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <TextField name="endDate"
                                                variant="outlined"
                                                value={endDate}
                                                fullWidth
                                                id="endDate"
                                                required
                                                onChange={this.handleChange.bind(this)}
                                                label="Дата конца"
                                                type="date"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={5}>
                                            <FormControl fullWidth required>
                                                <InputLabel htmlFor="select-priority">Приоритет</InputLabel>
                                                <Select
                                                    fullWidth
                                                    value={priority}
                                                    name="priority"
                                                    onChange={this.handleChange.bind(this)}
                                                    input={<Input id="select-priority" />}
                                                >
                                                    <MenuItem value={1}>1</MenuItem>
                                                    <MenuItem value={2}>2</MenuItem>
                                                    <MenuItem value={3}>3</MenuItem>
                                                    <MenuItem value={4}>4</MenuItem>
                                                    <MenuItem value={5}>5</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                type="submit"
                                                color="primary"
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Создать
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Button fullWidth
                                                onClick={this.handleClick}
                                                variant="contained"
                                                className={classes.submit}
                                            >
                                                Отменить
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </Container>
                    </div>
                )
            )
    }
}

CreateProjectPage.propTypes = {
    classes: PropTypes.object.isRequired
};
export default withStyles(CreateFormTheme)(CreateProjectPage);
