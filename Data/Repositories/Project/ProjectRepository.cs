﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.DbContext;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories.Project
{
    public class ProjectRepository : Repository<Entities.Project>, IProjectRepository
    {
        public ProjectRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<Entities.Project> FindByProjectNameAsync(string name)
        {
            return await Context.Projects.FirstOrDefaultAsync(c => c.ProjectName == name);
        }

        public async Task<List<Entities.Project>> GetAllByName(string name)
        {
            return await Context.Projects.Where(c => c.ProjectName == name).ToListAsync();
        }

        public async Task<List<Entities.Project>> GetAllByCustomerName(string customerCompanyName)
        {
            return await Context.Projects.Where(c => c.CompanyCustomer == customerCompanyName).ToListAsync();
        }

        public async Task<List<Entities.Project>> GetAllByExecutingName(string executingCompanyName)
        {
            return await Context.Projects.Where(c => c.CompanyPerformer == executingCompanyName).ToListAsync();
        }

        public async Task<List<Entities.Project>> GetAllByDate(DateTime? beginDate, DateTime? endDate)
        {
            return await Context.Projects.Where(c => c.BeginTime >= beginDate && c.EndTime <= endDate).ToListAsync();
        }

        public async Task<List<Entities.Project>> GetAllByProjectManagerName(string managerName)
        {
            return await Context.Projects.Where(c => c.ProjectManager == managerName).ToListAsync();
        }

        public async Task<List<Entities.Project>> GetAllByPriority(int? priority)
        {
          return await Context.Projects.Where(c => c.Priority == priority).ToListAsync();
        }
    }
}
