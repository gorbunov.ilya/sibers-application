﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Repositories.Project
{
   public interface IProjectRepository : IRepository<Entities.Project>
   {
       Task<Entities.Project> FindByProjectNameAsync(string name);
       Task<List<Entities.Project>> GetAllByName(string name);
        Task<List<Entities.Project>> GetAllByCustomerName(string customerCompanyName);
        Task<List<Entities.Project>> GetAllByExecutingName(string filterExecutingCompanyName);
        Task<List<Entities.Project>> GetAllByDate(DateTime? beginDate, DateTime? endDate);
        Task<List<Entities.Project>> GetAllByProjectManagerName(string managerName);
        Task<List<Entities.Project>> GetAllByPriority(int? priority);
    }
}
