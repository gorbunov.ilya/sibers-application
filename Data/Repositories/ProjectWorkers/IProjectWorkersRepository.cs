﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.ProjectWorkers
{
    public interface IProjectWorkersRepository : IRepository<Entities.ProjectWorkers>
    {
        Task<Entities.ProjectWorkers> FindByWorkerIdAsync(int id);
        Task CreateRangeAsync(Entities.ProjectWorkers[] projectWorkers);
    }
}
