﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.DbContext;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories.ProjectWorkers
{
    public class ProjectWorkersRepository : Repository<Entities.ProjectWorkers>, IProjectWorkersRepository
    {
        public ProjectWorkersRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<Entities.ProjectWorkers> FindByWorkerIdAsync(int id)
        {
            return await Context.ProjectWorkers.FirstOrDefaultAsync(c => c.WorkerId == id);
        }

        public async Task CreateRangeAsync(Entities.ProjectWorkers[] projectWorkers)
        {
            await Context.AddRangeAsync(projectWorkers);
        }
    }
}
