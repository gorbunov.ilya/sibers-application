﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Repositories.Worker
{
    public interface IWorkerRepository : IRepository<Entities.Worker>
    {
        Task<Entities.Worker> FindByFullNameAsync(Entities.Worker worker);
        Task<List<Entities.Worker>> FindByName(string name);
        Task<List<Entities.Worker>> FindByEmail(string email);
        Task<List<Entities.Worker>> FindBySurname(string surname);
    }
}
