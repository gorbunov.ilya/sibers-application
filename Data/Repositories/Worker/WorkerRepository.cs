﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.DbContext;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories.Worker
{
    public class WorkerRepository : Repository<Entities.Worker>, IWorkerRepository
    {
        public WorkerRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<Entities.Worker> FindByFullNameAsync(Entities.Worker worker)
        {
            return await Context.Workers
                .FirstOrDefaultAsync(c => c.Name == worker.Name 
                                                                  && c.Surname == worker.Surname 
                                                                  && c.MiddleName == worker.MiddleName);
        }

        public async Task<List<Entities.Worker>> FindByName(string name)
        {
            return await Context.Workers
                .Where(c => c.Name == name).ToListAsync();
        }

        public async Task<List<Entities.Worker>> FindByEmail(string email)
        {
            return await Context.Workers
                .Where(c => c.Email == email).ToListAsync();
        }

        public async Task<List<Entities.Worker>> FindBySurname(string surname)
        {
            return await Context.Workers
                .Where(c => c.Surname == surname).ToListAsync();
        }
    }
}
