﻿using System;
using System.Threading.Tasks;
using Data.Entities;
using Data.Repositories;
using Data.Repositories.Project;
using Data.Repositories.ProjectWorkers;
using Data.Repositories.Worker;

namespace Data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IProjectRepository Project { get; }
        IProjectWorkersRepository ProjectWorkers{ get; }
        IWorkerRepository Worker { get; }

        Task<int> CompleteAsync();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;
    }
}
