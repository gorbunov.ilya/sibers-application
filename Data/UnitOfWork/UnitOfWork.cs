﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.DbContext;
using Data.Entities;
using Data.Repositories;
using Data.Repositories.Project;
using Data.Repositories.ProjectWorkers;
using Data.Repositories.Worker;

namespace Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ConcurrentDictionary<Type, object> _repositories;

        public IProjectRepository Project { get; }
        public IProjectWorkersRepository ProjectWorkers { get; }
        public IWorkerRepository Worker { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositories = new ConcurrentDictionary<Type, object>();

            Project = new ProjectRepository(_context);
            ProjectWorkers = new ProjectWorkersRepository(_context);
            Worker = new WorkerRepository(_context);
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            return _repositories.GetOrAdd(typeof(TEntity), new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
