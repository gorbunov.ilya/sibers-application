﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.DbContext;

namespace Data.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly IApplicationDbContextFactory _contextFactory;

        public UnitOfWorkFactory(IApplicationDbContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public IUnitOfWork MakeUnitOfWork()
        {
            return new UnitOfWork(_contextFactory.CreateContext());
        }
    }
}
