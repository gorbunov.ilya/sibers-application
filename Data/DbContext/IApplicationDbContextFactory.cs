﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.DbContext
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext CreateContext();
    }
}
