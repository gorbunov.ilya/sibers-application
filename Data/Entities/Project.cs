﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entities
{
    public class Project : BaseEntity
    {
        [Required]
        public string ProjectName { get; set; }
        [Required]
        public string CompanyCustomer { get; set; }
        [Required]
        public string CompanyPerformer { get; set; }
        [Required]
        public string ProjectManager { get; set; }
        [Required]
        public int Priority { get; set; }
        [Required]
        public DateTime BeginTime { get; set; }
        [Required]
        public DateTime EndTime { get; set; }
        public ICollection<ProjectWorkers> ProjectWorkers { get; set; }
    }
}
