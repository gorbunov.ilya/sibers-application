﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class ProjectWorkers : BaseEntity
    {
        public int WorkerId { get; set; }
        public Worker Worker { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
