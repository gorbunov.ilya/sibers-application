﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Filters;
using Data.Entities;
using Data.UnitOfWork;

namespace BusinessLogic.Services
{
    public class ProjectService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public ProjectService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        public async Task<OperationResult> CreateProjectAsync(Project project)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            var foundProject = await unitOfWork.Project.FindByProjectNameAsync(project.ProjectName);
            if (foundProject == null)
            {
                await unitOfWork.Project.CreateAsync(project);
                var result = await unitOfWork.CompleteAsync();
                if (result != 0)
                {
                    return new OperationResult(true, "Project created");
                }
                return new OperationResult(false, "An error occurred while creating");

            }
            return new OperationResult(false, "A project with the same name already exists.");

        }

        public async Task<OperationResult> EditProjectAsync(Project project)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork(); 
            unitOfWork.Project.Update(project);
           var result = await unitOfWork.CompleteAsync();
           if (result != 0)
           {
               return new OperationResult(true, "Successfully updated.");
           }
           return  new OperationResult(false, "An error occurred while updating");
            
        }

        public async Task<OperationResult> DeleteProjectAsync(Project project)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            unitOfWork.Project.Delete(project);
            await unitOfWork.CompleteAsync();
            return new OperationResult(true, "Project successfully deleted");
        }

        public async Task<List<Project>> GetAllProjectsAsync(ProjectFilter filter)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            List<Project> projects = await unitOfWork.Project.GetAllAsync();
            if (filter.Name != null)
            {
                projects = await unitOfWork.Project.GetAllByName(filter.Name);
            }
            if (filter.CustomerCompanyName != null)
            {
                projects = await unitOfWork.Project.GetAllByCustomerName(filter.CustomerCompanyName);
            }
            if (filter.ExecutingCompanyName != null)
            {
                projects = await unitOfWork.Project.GetAllByExecutingName(filter.ExecutingCompanyName);
            }

            if (filter.BeginDate != null && filter.EndDate != null)
            {
                projects = await unitOfWork.Project.GetAllByDate(filter.BeginDate, filter.EndDate);
            }

            if (filter.ManagerName != null)
            {
                projects = await unitOfWork.Project.GetAllByProjectManagerName(filter.ManagerName);
            }

            if (filter.Priority != null)
            {
                projects = await unitOfWork.Project.GetAllByPriority(filter.Priority);
            }
            return projects;
        }

        public async Task<Project> GetProjectById(int id)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            return await unitOfWork.Project.GetByIdAsync(id);
        }
    }
}
