﻿using Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Common;
using Common.Filters;

namespace BusinessLogic.Services
{
    public class WorkerService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public WorkerService(IUnitOfWorkFactory unitOfWork)
        {
            _unitOfWorkFactory = unitOfWork;
        }

        public async Task<OperationResult> CreateWorkerAsync(Worker worker)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            var foundWorker = await unitOfWork.Worker.FindByFullNameAsync(worker);
            if (foundWorker == null)
            {
                await unitOfWork.Worker.CreateAsync(worker);
                await unitOfWork.CompleteAsync();
                return new OperationResult(true, "Worker Created");
            }
            return  new OperationResult(false, "An employee with such a full name already exists");
        }

        public async Task<OperationResult> EditWorkerAsync(Worker worker)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            unitOfWork.Worker.Update(worker);
            await unitOfWork.CompleteAsync();
            return new OperationResult(true, "Data updated");
        }

        public async Task<OperationResult> DeleteWorkerAsync(Worker worker)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            var projectWorker = await unitOfWork.ProjectWorkers.FindByWorkerIdAsync(worker.Id);
            if (projectWorker == null)
            {
                unitOfWork.Worker.Delete(worker);
                await unitOfWork.CompleteAsync();
                return new OperationResult(true, "Worker deleted");
            }
            return  new OperationResult(false, "The employee cannot be removed," +
                                               "as involved in the project.");
        }

        public async Task<List<Worker>> GetAllWorkersAsync(WorkerFilter filter)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            var workers = await unitOfWork.Worker.GetAllAsync();
            if (filter.Name != null)
            {
                workers = await unitOfWork.Worker.FindByName(filter.Name);
            }

            if (filter.Email != null)
            {
                workers = await unitOfWork.Worker.FindByEmail(filter.Email);
            }

            if (filter.Surname != null)
            {
                workers = await unitOfWork.Worker.FindBySurname(filter.Name);
            }
            return workers;
        }
    }
}
