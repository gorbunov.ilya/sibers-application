﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Common;
using Data.Entities;
using Data.UnitOfWork;

namespace BusinessLogic.Services
{
    public class ProjectWorkersService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public ProjectWorkersService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<OperationResult> AddWorkersToProjectAsync(ProjectWorkersDto dto)
        {
            ProjectWorkers[] projectWorkers = new ProjectWorkers[dto.EmployeesId.Count];
            for (int i = 0; i < dto.EmployeesId.Count; i++)
            {
                projectWorkers[i] = new ProjectWorkers()
                {
                    WorkerId = dto.EmployeesId[i],
                    ProjectId =  dto.ProjectId
                    
                };
            }
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            await unitOfWork.ProjectWorkers.CreateRangeAsync(projectWorkers);
            var result = await unitOfWork.CompleteAsync();

            return result != 0 ? new OperationResult(true, "Employees added successfully") : 
                new OperationResult(false, "An error occurred while adding");
        }
        public async Task<OperationResult> DeleteWorkerFromProjectAsync(ProjectWorkers worker)
        {
            using var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork();
            unitOfWork.ProjectWorkers.Delete(worker);
            int result = await unitOfWork.CompleteAsync();
            return result != 0 ? new OperationResult(true, "Employee deleted") :
                new OperationResult(false, "An error occurred while uninstalling");
        }
    }
}
