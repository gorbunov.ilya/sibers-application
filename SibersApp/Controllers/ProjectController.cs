﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Common.Filters;
using Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SibersApp.Controllers
{
    public class ProjectController : Controller
    {
        private readonly ProjectService _service;

        public ProjectController(ProjectService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("api/createProject")]
        public async Task<IActionResult> CreateProjectAsync([FromBody]Project project)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.CreateProjectAsync(project);
                if (result._result)
                {
                    return Ok(result._message);
                }
                return BadRequest(result._message);
            }
            return BadRequest("The project could not be created. " +
                              "Check that the data set is correct.");
        }

        [HttpPost]
        [Route("api/editProject")]
        public async Task<IActionResult> EditProjectAsync(Project project)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.EditProjectAsync(project);
                if (result._result)
                {
                    return Ok(result._message);
                }
                return BadRequest(result._message);
            }
            return BadRequest("The project cannot be changed." +
                              "Verify the data set is correct");
        }
        [HttpPost]
        [Route("api/deleteProject")]
        public async Task<IActionResult> DeleteProjectAsync([FromBody]Project project)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.DeleteProjectAsync(project);
                if (result._result)
                {
                    return Ok(result._message);
                }
                return BadRequest(result._message);
            }
            return BadRequest("The project cannot be deleted." +
                              "Verify the data set is correct");
        }

        [HttpGet]
        [Route("api/getAllProjects")]
        public async Task<IActionResult> GetAllProjectsAsync([FromQuery]ProjectFilter filter)
        {
            return Ok(await _service.GetAllProjectsAsync(filter));
        }
        [HttpGet]
        [Route("api/getProject")]
        public async Task<IActionResult> GetProjectById([FromQuery] int id)
        {
            return Ok(await _service.GetProjectById(id));
        }
    }
}