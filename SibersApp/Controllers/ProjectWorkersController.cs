﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Common;
using Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SibersApp.Controllers
{
    public class ProjectWorkersController : Controller
    {
        private readonly ProjectWorkersService _service;

        public ProjectWorkersController(ProjectWorkersService service)
        {
            _service = service;
        }

        [HttpPost]
        [Route("/api/addWorkersToProject")]
        public async Task<IActionResult> AddWorkersToProjectAsync([FromBody] ProjectWorkersDto dto)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.AddWorkersToProjectAsync(dto);
                return result._result ? StatusCode(200, result._message) : BadRequest(result._message);

            }
            return BadRequest("Upload error");
        }
        [HttpPost]
        [Route("/api/deleteWorkersFromProject")]
        public async Task<IActionResult> DeleteWorkersToProjectAsync(ProjectWorkers worker)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.DeleteWorkerFromProjectAsync(worker);
                return result._result ? StatusCode(200, result._message) : BadRequest(result._message);

            }
            return BadRequest("Delete error, data is not correct");
        }
    }
}