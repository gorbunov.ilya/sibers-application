﻿using System.Threading.Tasks;
using BusinessLogic.Services;
using Common.Filters;
using Data.Entities;
using Microsoft.AspNetCore.Mvc;

namespace SibersApp.Controllers
{
    public class WorkerController : Controller
    {
        private readonly WorkerService _service;

        public WorkerController(WorkerService service)
        {
            _service = service;
        }
        [HttpPost]
        [Route("api/createWorker")]
        public async Task<IActionResult> CreateWorkerAsync(Worker worker)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.CreateWorkerAsync(worker);
                if (result._result)
                {
                    return Ok(result._message);
                }
                return BadRequest(result._message);
            }
            return BadRequest("Worker cannot be created." +
                              "Verify the data set is correct");
        }
        [HttpPost]
        [Route("api/deleteWorker")]
        public async Task<IActionResult> DeleteWorkerAsync(Worker worker)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.DeleteWorkerAsync(worker);
                if (result._result)
                {
                    return Ok(result._message);
                }
                return BadRequest(result._message);
            }
            return BadRequest("The employee cannot be removed." +
                              "Verify the data set is correct");
        }
        [HttpPost]
        [Route("api/editWorker")]
        public async Task<IActionResult> EditWorkerAsync(Worker worker)
        {
            if (ModelState.IsValid)
            {
                var result = await _service.EditWorkerAsync(worker);
                if (result._result)
                {
                    return Ok(result._message);
                }
                return BadRequest(result._message);
            }
            return BadRequest("The employee cannot be changed." +
                              "Verify the data set is correct");
        }
        [HttpGet]
        [Route("api/getAllWorkers")]
        public async Task<IActionResult> GetAllWorkersAsync([FromQuery] WorkerFilter filter)
        {
            return Ok(await _service.GetAllWorkersAsync(filter));
        }


    }
}