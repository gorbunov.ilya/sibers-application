﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Filters
{
    public class ProjectFilter
    {
        public string ManagerName { get; set; }
        public string Name { get; set; }
        public string CustomerCompanyName { get; set; }
        public string ExecutingCompanyName { get; set; }
        public int? Priority { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
