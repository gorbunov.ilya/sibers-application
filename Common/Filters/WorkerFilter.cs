﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Filters
{
    public class WorkerFilter
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
    }
}
