﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common
{
    public class ProjectWorkersDto : BaseEntity
    {
        [Required]
        public int ProjectId { get; set; }
        public List<int> EmployeesId { get; set; }
    }
}
