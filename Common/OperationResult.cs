﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class OperationResult
    {
        public readonly bool _result;
        public readonly string _message;

        public OperationResult(bool result, string message)
        {
            _result = result;
            _message = message;
        }
    }
}
